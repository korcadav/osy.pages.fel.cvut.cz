#include <unistd.h>

int main()
{
    int syscall_num = 4;
    int fd = 1;
    char *ptr = "Hello World\n";
    int len = 12;
    asm volatile("int $0x80"
                 :
                 : "a"(syscall_num), "b"(fd), "c"(ptr), "d"(len)
                 : "memory");
    return 0;
}
