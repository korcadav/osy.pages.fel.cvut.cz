#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int a;
volatile int turn;
volatile int interest[2];

void *fce(void *n)
{
    int i;
    int j = *(int *)n;
    for (i = 0; i < 1000000; i++) {
        interest[j] = 1;
        __sync_synchronize(); /* memory barrier */
        turn = (1 - j);
        while (interest[1 - j] == 1 && turn == (1 - j))
            ; /* repeat and wait */
        a += 1;
        interest[j] = 0;
    }
    printf("a=%i\n", a);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t tid1, tid2;
    int i = 0;
    int j = 1;
    interest[0] = interest[1] = 0;

    a = 0;
    turn = 0;
    pthread_create(&tid1, NULL, fce, &i);
    pthread_create(&tid2, NULL, fce, &j);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    if (a == 2000000) {
        printf("OK\n");
    } else {
        printf("Fail %i\n", a);
    }
    return (a == 2000000) ? 0 : 1;
}
