use std::cell::RefCell;
use std::convert::TryInto;
use std::mem::MaybeUninit;

use nix::libc::{sem_destroy, sem_init, sem_post, sem_t, sem_wait};

pub struct Semaphore {
    sem: RefCell<sem_t>,
}
unsafe impl Send for Semaphore {}
unsafe impl Sync for Semaphore {}

impl Semaphore {
    pub fn new(init_val: usize) -> Result<Self, i32> {
        let mut s = MaybeUninit::uninit();
        let s = unsafe {
            let init_return = sem_init(s.as_mut_ptr(), 0, init_val.try_into().unwrap());
            if init_return != 0 {
                return Err(init_return);
            }
            s.assume_init()
        };

        Ok(Semaphore {
            sem: RefCell::new(s),
        })
    }

    pub fn wait(&self) {
        unsafe {
            sem_wait(self.sem.as_ptr());
        }
    }

    pub fn post(&self) {
        unsafe {
            sem_post(self.sem.as_ptr());
        }
    }
}

impl Drop for Semaphore {
    fn drop(&mut self) {
        unsafe {
            // sem is always a valid semaphore
            sem_destroy(self.sem.as_ptr());
        }
    }
}
